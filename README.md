# fr-email-templates

Repository for storing various template work, including HTML email templates, Acoustic Dynamic Content code and more.

Main branch will not be used for the fr-email-templates repository. Keep empty and use only for creating new empty branches. Personal work should be stored on your named branch (e.g. [templates-ppenloglou](https://gitlab.wikimedia.org/ppenloglou/fr-email-templates/-/tree/templates-ppenloglou)). If you don't have a personal branch yet, reach out to ppenloglou@wikimedia.org.

Our shared work will be stored in the [templates-shared](https://gitlab.wikimedia.org/ppenloglou/fr-email-templates/-/tree/templates-shared) branch.
